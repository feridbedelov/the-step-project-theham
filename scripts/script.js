$(document).ready(function () {
    $(".navigation .nav-item").click(function () {
        $(this).addClass("active").siblings().removeClass("active");
    });

    $(".tabs .tab-contents li").click(function () {
        $(this).addClass("active").siblings().removeClass("active").closest(".tabs").find(".tab-content").removeClass("active").eq($(this).index()).addClass("active");
    });

    $(".filter .captions li").click(function () {
        $(this).addClass("active").siblings().removeClass("active").closest(".filter");

        let category  = $(this).attr('data-category');

        $(this).closest(".filter").find(".content img").each(function(i, item) {
            if ($(item).attr('data-category') == category || category == "all" ) {
                $(item).show();
            } else {
                $(item).hide();
            }
        })

    });

    $(".our-work .btn.load").click(function () {
        loadWorks();
        if (loadedWorksCount >= worksLibrary.length) {
            $(this).hide();
        }
    });
    randomizeArray(worksLibrary);
    loadWorks();
    let slide = new Slider("#testimonial");

});







let worksLibrary = [
	{category: "graphic-design", caption: "Graphic Design", file: "/graphic design/graphic-design1.jpg"},
	{category: "graphic-design", caption: "Graphic Design", file: "/graphic design/graphic-design2.jpg"},
	{category: "graphic-design", caption: "Graphic Design", file: "/graphic design/graphic-design3.jpg"},
	{category: "graphic-design", caption: "Graphic Design", file: "/graphic design/graphic-design4.jpg"},
	{category: "graphic-design", caption: "Graphic Design", file: "/graphic design/graphic-design5.jpg"},
	{category: "graphic-design", caption: "Graphic Design", file: "/graphic design/graphic-design6.jpg"},
	{category: "graphic-design", caption: "Graphic Design", file: "/graphic design/graphic-design7.jpg"},
	{category: "graphic-design", caption: "Graphic Design", file: "/graphic design/graphic-design8.jpg"},
	{category: "graphic-design", caption: "Graphic Design", file: "/graphic design/graphic-design9.jpg"},
	{category: "graphic-design", caption: "Graphic Design", file: "/graphic design/graphic-design10.jpg"},
	{category: "graphic-design", caption: "Graphic Design", file: "/graphic design/graphic-design11.jpg"},
	{category: "graphic-design", caption: "Graphic Design", file: "/graphic design/graphic-design12.jpg"},
	{category: "landing-page", caption: "Landing Page", file: "/landing page/landing-page1.jpg"},
	{category: "landing-page", caption: "Landing Page", file: "/landing page/landing-page2.jpg"},
	{category: "landing-page", caption: "Landing Page", file: "/landing page/landing-page3.jpg"},
	{category: "landing-page", caption: "Landing Page", file: "/landing page/landing-page4.jpg"},
	{category: "landing-page", caption: "Landing Page", file: "/landing page/landing-page5.jpg"},
	{category: "landing-page", caption: "Landing Page", file: "/landing page/landing-page6.jpg"},
	{category: "landing-page", caption: "Landing Page", file: "/landing page/landing-page7.jpg"},
	{category: "web-design", caption: "Web Design", file: "/web design/web-design1.jpg"},
	{category: "web-design", caption: "Web Design", file: "/web design/web-design2.jpg"},
	{category: "web-design", caption: "Web Design", file: "/web design/web-design3.jpg"},
	{category: "web-design", caption: "Web Design", file: "/web design/web-design4.jpg"},
	{category: "web-design", caption: "Web Design", file: "/web design/web-design5.jpg"},
	{category: "web-design", caption: "Web Design", file: "/web design/web-design6.jpg"},
	{category: "web-design", caption: "Web Design", file: "/web design/web-design7.jpg"},
	{category: "wordpress", caption: "WordPress", file: "/wordpress/wordpress1.jpg"},
	{category: "wordpress", caption: "WordPress", file: "/wordpress/wordpress2.jpg"},
	{category: "wordpress", caption: "WordPress", file: "/wordpress/wordpress3.jpg"},
	{category: "wordpress", caption: "WordPress", file: "/wordpress/wordpress4.jpg"},
	{category: "wordpress", caption: "WordPress", file: "/wordpress/wordpress5.jpg"},
	{category: "wordpress", caption: "WordPress", file: "/wordpress/wordpress6.jpg"},
	{category: "wordpress", caption: "WordPress", file: "/wordpress/wordpress7.jpg"},
	{category: "wordpress", caption: "WordPress", file: "/wordpress/wordpress8.jpg"},
	{category: "wordpress", caption: "WordPress", file: "/wordpress/wordpress9.jpg"},
	{category: "wordpress", caption: "WordPress", file: "/wordpress/wordpress10.jpg"}
];


let loadedWorksCount = 0;
let loadingWorksPortion = 12;

function loadWorks() {
	if (loadedWorksCount < worksLibrary.length) {
		let tmp = loadedWorksCount < worksLibrary.length - loadingWorksPortion ? loadedWorksCount + loadingWorksPortion : worksLibrary.length;
		for (let i = loadedWorksCount; i < tmp; i++) {

			let itemContainer = $("<div>").addClass("outer");
			$(".our-work .content").append(itemContainer);


			let item = $("<img />")
				.attr("src", "img" + worksLibrary[i].file)
				.attr("data-category", worksLibrary[i].category)
				.attr("draggable", false);
			$(itemContainer ).append(item);

			let itemInfo = $("<div>").addClass("info")
				.append("<div class='info-btn-outer'>" +
					"<div class='info-btn link'>" +
						"<i class='fa fa-link' aria-hidden='true'></i>" +
					"</div>" +
					"<div class='info-btn search'>" +
						"<i class='fa fa-search' aria-hidden='true'></i>" +
					"</div>" +
					"</div>")
				.append("<p class='title'>creative design</p>")
				.append("<p class='caption'>" + worksLibrary[i].caption + "</p>");
			$(itemContainer).append(itemInfo);

			loadedWorksCount++;
		}
	}
}


function randomizeArray(arr) {
	arr.forEach(function (item) {
		item.sortOrder = Math.random();
	});
	arr = arr.sort(function (o1, o2) {
		return o1.sortOrder - o2.sortOrder;
	});
}




function Slider(elementSelector) {

	this.items = [
		{
			text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
			name: "Worker113", title: "Developer",
			img: "img/photos/photo1.png"
		},
		{
			text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
			name: "Worker22", title: "Tester",
			img: "img/photos/photo2.png"
		},
		{
			text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
			name: "Worker11", title: "UX Designer",
			img: "img/photos/photo3.png"
		},
		{
			text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
			name: "Worker", title: "Director",
			img: "img/photos/photo4.png"
		},
		{
			text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
			name: "Worker 2", title: "Boss",
			img: "img/photos/photo1.png"
		},
		{
			text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
			name: "Worker 3", title: "Manager",
			img: "img/photos/photo3.png"
		}
	];

	this.scrolledItemsCount = 3;

	this.selectedItemIndex = 0;
	this.scrollOffset = 0;

	this.init = () => {
		this.elementSlider = $(elementSelector).addClass("slider");

		this.elementText = $("<p></p>").addClass("text");
		$(this.elementSlider).append(this.elementText);

		this.elementName = $("<p></p>").addClass("name");
		$(this.elementSlider).append(this.elementName);

		this.elementTitle = $("<p></p>").addClass("title");
		$(this.elementSlider).append(this.elementTitle);

		this.elementImgContainer = $("<div>").addClass("img-container");
		$(this.elementSlider).append(this.elementImgContainer);

		this.elementImg = $("<img>").addClass("img");
		$(this.elementImgContainer).append(this.elementImg);

		this.elementScrollContainer = $("<div>").addClass("scroll-container");
		$(this.elementSlider).append(this.elementScrollContainer);

		this.elementBtnPrev = $("<div>").addClass("slider-btn prev");
		$(this.elementBtnPrev).prop("disabled", true);
		$(this.elementScrollContainer).append(this.elementBtnPrev);

		this.elementListWindow = $("<div>").addClass("window");
		$(this.elementScrollContainer).append(this.elementListWindow);
		$(this.elementListWindow).click(this.onItemClick);

		this.elementList = $("<div>").addClass("list");
		$(this.elementListWindow).append(this.elementList);

		this.elementBtnNext = $("<div>").addClass("slider-btn next");
		$(this.elementScrollContainer).append(this.elementBtnNext);

		$(this.elementBtnPrev).click(this.scrollRight);

		$(this.elementBtnNext).click(this.scrollLeft);
	};

	this.addItems = () => {
		let self = this;
		this.itemWidth = 0;
		$(this.items).each(function (i, item) {

			let elementItemContainer = $("<div>").addClass("item-container");
			$(self.elementList).append(elementItemContainer);

			let elementItem = $("<img>").addClass("item");
			$(elementItem)
				.attr("src", item.img)
				.attr("data-text", item.text)
				.attr("data-name", item.name)
				.attr("data-title", item.title)
				.attr("data-img", item.img);
			$(elementItemContainer).append(elementItem);

			if (i == 0) {
				self.itemWidth = $(elementItemContainer).outerWidth(true);
				self.showInfo(elementItem);
			}
		});
		$(this.elementListWindow).width(this.itemWidth * this.scrolledItemsCount);
	};

	this.scrollItems = function (offset) {
		$(this.elementList).css("left", this.itemWidth * offset);
	};

	this.scrollRight = () => {
		if (this.scrollOffset < 0) {
			this.scrollItems(++this.scrollOffset);
		}
	};

	this.scrollLeft = () => {
		if (this.scrollOffset > -(this.items.length - this.scrolledItemsCount)) {
			this.scrollItems(--this.scrollOffset);
		}
	};

	this.showInfo = (element) => {
		this.elementText.text($(element).attr("data-text"));
		this.elementName.text($(element).attr("data-name"));
		this.elementTitle.text($(element).attr("data-title"));
		$(this.elementImg).attr("src", $(element).attr("data-img"))
	};

	this.onItemClick = (event) => {
		let element = document.elementFromPoint(event.clientX, event.clientY);
		if (element != null && element.classList.contains("item")) {
			this.showInfo(element);
		}
	};

	this.init();
	this.addItems();
}



